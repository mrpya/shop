<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('dashboard/css/bootstrap-theme.css') }}">
    <!-- Bootstrap rtl -->
    <link rel="stylesheet" href="{{ asset('dashboard/css/rtl.css') }}">
    <!-- persian Date Picker -->
    <link rel="stylesheet" href="{{ asset('dashboard/css/persian-datepicker-0.4.5.min') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('dashboard/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('dashboard/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dashboard/css/AdminLTE.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('dashboard/css/skins/_all-skins.min.css') }}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{ asset('dashboard/morris.js/morris.css') }}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('dashboard/jvectormap/jquery-jvectormap.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('dashboard/bootstrap-daterangepicker/daterangepicker.css') }}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ asset('dashboard/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body class="skin-blue layout-boxed sidebar-mini">
    <div class="wrapper">

        <!-- Main Header -->
        <header class="main-header">
          <!-- Logo -->
          <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">SHOP</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>SHOP</b></span>
          </a>
          <!-- Header Navbar: style can be found in header.less -->
          <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
              <span class="sr-only">Toggle navigation</span>
            </a>
      
      
            <!-- Delete This after download -->
            <a href="#" class="btn hidden-xs" style="margin:6px 100px;padding:9px 50px;background-color:#d61577;border-color:#ad0b5d;font-weight:bold;color:#FFF">SHOP</a>
            <!-- End Delete-->
      
      
      
            <div class="navbar-custom-menu">
              <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-envelope-o"></i>
                    <span class="label label-success">4</span>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="header">۴ پیام خوانده نشده</li>
                    <li>
                      <!-- inner menu: contains the actual data -->
                      <ul class="menu">
                        <li><!-- start message -->
                          <a href="#">
                            <div class="pull-left">
                              <img src="{{ asset('dashboard/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
                            </div>
                            <h4>
                              علیرضا
                              <small><i class="fa fa-clock-o"></i> ۵ دقیقه پیش</small>
                            </h4>
                            <p>متن پیام</p>
                          </a>
                        </li>
                        <!-- end message -->
                        <li>
                          <a href="#">
                            <div class="pull-left">
                              <img src="{{ asset('dashboard/img/user2-160x160.jpg') }}"" class="img-circle" alt="User Image">
                            </div>
                            <h4>
                              نگین
                              <small><i class="fa fa-clock-o"></i> ۲ ساعت پیش</small>
                            </h4>
                            <p>متن پیام</p>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <div class="pull-left">
                              <img src="{{ asset('dashboard/img/user2-160x160.jpg') }}"" class="img-circle" alt="User Image">
                            </div>
                            <h4>
                              نسترن
                              <small><i class="fa fa-clock-o"></i> امروز</small>
                            </h4>
                            <p>متن پیام</p>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <div class="pull-left">
                              <img src="{{ asset('dashboard/img/user2-160x160.jpg') }}"" class="img-circle" alt="User Image">
                            </div>
                            <h4>
                              نگین
                              <small><i class="fa fa-clock-o"></i> دیروز</small>
                            </h4>
                            <p>متن پیام</p>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <div class="pull-left">
                              <img src="{{ asset('dashboard/img/user2-160x160.jpg') }}"" class="img-circle" alt="User Image">
                            </div>
                            <h4>
                              نسترن
                              <small><i class="fa fa-clock-o"></i> ۲ روز پیش</small>
                            </h4>
                            <p>متن پیام</p>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="footer"><a href="#">نمایش تمام پیام ها</a></li>
                  </ul>
                </li>
                <!-- Notifications: style can be found in dropdown.less -->
                <li class="dropdown notifications-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bell-o"></i>
                    <span class="label label-warning">۱۰</span>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="header">۱۰ اعلان جدید</li>
                    <li>
                      <!-- inner menu: contains the actual data -->
                      <ul class="menu">
                        <li>
                          <a href="#">
                            <i class="fa fa-users text-aqua"></i> ۵ کاربر جدید ثبت نام کردند
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-warning text-yellow"></i> اخطار دقت کنید
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-users text-red"></i> ۴ کاربر جدید ثبت نام کردند
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-shopping-cart text-green"></i> ۲۵ سفارش جدید
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-user text-red"></i> نام کاربریتان را تغییر دادید
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="footer"><a href="#">نمایش همه</a></li>
                  </ul>
                </li>
                <!-- Tasks: style can be found in dropdown.less -->
                <li class="dropdown tasks-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-flag-o"></i>
                    <span class="label label-danger">۹</span>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="header">۹ کار برای انجام دارید</li>
                    <li>
                      <!-- inner menu: contains the actual data -->
                      <ul class="menu">
                        <li><!-- Task item -->
                          <a href="#">
                            <h3>
                              ساخت دکمه
                              <small class="pull-right">20%</small>
                            </h3>
                            <div class="progress xs">
                              <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                                   aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only">20% تکمیل شده</span>
                              </div>
                            </div>
                          </a>
                        </li>
                        <!-- end task item -->
                        <li><!-- Task item -->
                          <a href="#">
                            <h3>
                              ساخت قالب جدید
                              <small class="pull-right">40%</small>
                            </h3>
                            <div class="progress xs">
                              <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar"
                                   aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only">40% تکمیل شده</span>
                              </div>
                            </div>
                          </a>
                        </li>
                        <!-- end task item -->
                        <li><!-- Task item -->
                          <a href="#">
                            <h3>
                              تبلیغات
                              <small class="pull-right">60%</small>
                            </h3>
                            <div class="progress xs">
                              <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar"
                                   aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only">60% تکمیل شده</span>
                              </div>
                            </div>
                          </a>
                        </li>
                        <!-- end task item -->
                        <li><!-- Task item -->
                          <a href="#">
                            <h3>
                              ساخت صفحه فرود
                              <small class="pull-right">80%</small>
                            </h3>
                            <div class="progress xs">
                              <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar"
                                   aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only">80% تکمیل شده</span>
                              </div>
                            </div>
                          </a>
                        </li>
                        <!-- end task item -->
                      </ul>
                    </li>
                    <li class="footer">
                      <a href="#">نمایش همه</a>
                    </li>
                  </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ asset('dashboard/img/user2-160x160.jpg') }}"" class="user-image" alt="User Image">
                    <span class="hidden-xs">علیرضا حسینی زاده</span>
                  </a>
                  <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                      <img src="{{ asset('dashboard/img/user2-160x160.jpg') }}"" class="img-circle" alt="User Image">
      
                      <p>
                        علیرضا حسینی زاده
                        <small>مدیریت کل سایت</small>
                      </p>
                    </li>
                    <!-- Menu Body -->
                    <li class="user-body">
                      <div class="row">
                        <div class="col-xs-4 text-center">
                          <a href="#">صفحه من</a>
                        </div>
                        <div class="col-xs-4 text-center">
                          <a href="#">فروش</a>
                        </div>
                        <div class="col-xs-4 text-center">
                          <a href="#">دوستان</a>
                        </div>
                      </div>
                      <!-- /.row -->
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                      <div class="pull-left">
                        <a href="#" class="btn btn-default btn-flat">پروفایل</a>
                      </div>
                      <div class="pull-right">
                        <a href="#" class="btn btn-default btn-flat">خروج</a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </nav>
        </header>
        <!-- right side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
      
          <!-- sidebar: style can be found in sidebar.less -->
          <section class="sidebar">
      
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
              <div class="pull-right image">
                <img src="{{ asset('dashboard/img/user2-160x160.jpg') }}"" class="img-circle" alt="User Image">
              </div>
              <div class="pull-right info">
                <p>المستخدم</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> متصل</a>
              </div>
            </div>
      
            <!-- search form (Optional) -->
            <form action="#" method="get" class="sidebar-form">
              <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="بحث">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                  </span>
              </div>
            </form>
            <!-- /.search form -->
      
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
              <li class="header">تیتر</li>
              <!-- Optionally, you can add icons to the links -->
              <li class="active"><a href="#"><i class="fa fa-link"></i> <span>رابط</span></a></li>
              <li><a href="#"><i class="fa fa-link"></i> <span>رابط ۲</span></a></li>
              <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>قسم</span>
                  <span class="pull-left-container">
                      <i class="fa fa-angle-right pull-left"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#">رابط 1</a></li>
                  <li><a href="#">رابط 2</a></li>
                </ul>
              </li>
            </ul>
            <!-- /.sidebar-menu -->
          </section>
          <!-- /.sidebar -->
        </aside>
      
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->

          @yield('content')

          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
      
        <!-- Main Footer -->
        <footer class="main-footer text-left">
          <strong>Copyleft &copy; 2014-2017 <a href="#">Almsaeed Studio</a> & <a href="#">Alireza Hosseinizadeh</a></strong>
        </footer>
 
      </div>

    <!-- Scripts -->

    <!-- jQuery 3 -->
    <script src="{{ asset('dashboard/jquery/dist/jquery.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('dashboard/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('dashboard/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- Morris.js charts -->
    <script src="{{ asset('dashboard/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('dashboard/morris.js/morris.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('dashboard/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <!-- jvectormap -->
    <script src="{{ asset('dashboard/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('dashboard/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('dashboard/jquery-knob/dist/jquery.knob.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ asset('dashboard/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('dashboard/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- datepicker -->
    <script src="{{ asset('dashboard/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('dashboard/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('dashboard/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('dashboard/fastclick/lib/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dashboard/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('dashboard/js/pages/dashboard.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dashboard/js/demo.js') }}"></script>

</body>
</html>
